package pattern.flyweight;

public class FordMustang {
    private FordMustangBase mustangBase;
    private String radio;

    public FordMustang(FordMustangBase mustangBase, String radio) {
        System.out.println(this.getClass() + "constructor");
        this.mustangBase = mustangBase;
        this.radio = radio;
    }
}
