package solid.liskov.fix;

public interface FileWrite {
    void write(byte[] date);
}
