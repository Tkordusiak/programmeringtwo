package pattern.iterator;

public class PaymentList implements MyList <Payment>{

    private Payment[] payments;

    public PaymentList(Payment[] payments) {
        this.payments = payments;
    }

    @Override
    public MyIterator iterator() {
        return new PaymentIterator(payments);
    }
}
