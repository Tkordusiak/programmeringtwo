package pattern.prototyp;

import java.util.List;

public class Test {
    public static void main(String[] args) {

        // szablon
        ShoppingList shoppingList = new ShoppingList();
        shoppingList.loadData();

        // nowa lista zakupów
        try {
            ShoppingList zakupy1 = (ShoppingList) shoppingList.clone();

            List<String> listaZakupow = zakupy1.getShoppingList();
            listaZakupow.add("piwo");
            listaZakupow.add("vodka");

            listaZakupow.forEach(System.out::println);
        }catch (CloneNotSupportedException e){
            e.printStackTrace();
        }
    }


}
