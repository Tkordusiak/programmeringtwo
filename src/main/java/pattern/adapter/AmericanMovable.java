package pattern.adapter;

public interface AmericanMovable {
    double getSpeed();
}
