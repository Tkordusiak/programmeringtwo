package solid.srp.fixed;

import solid.srp.Person;

public class DrivingLicense {

    private static final int DRIVING_LICENSE_AGE = 18;

   static public boolean canGetDrivingLicense(Person person){
        return person.getAge()>DRIVING_LICENSE_AGE;
    }
}
