package bank;

public class Account {

    private String nameOfBank;
    private TypeOfAccount type;
    private Double balance;
    private  Integer numberOfAccount;

    public Account(String nameOfBank, TypeOfAccount typeOfAccount, Double balance, Integer numberOfAccount) {
        this.nameOfBank = nameOfBank;
        this.type = typeOfAccount;
        this.balance = balance;
        this.numberOfAccount = numberOfAccount;
    }

    public String getNameOfBank() {
        return nameOfBank;
    }

    public TypeOfAccount getType() {
        return type;
    }

    public Double getBalance() {
        return balance;
    }

    public Integer getNumberOfAccount() {
        return numberOfAccount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "nameOfBank='" + nameOfBank + '\'' +
                ", typeOfAccount=" + type +
                ", balance=" + balance +
                ", numberOfAccount=" + numberOfAccount +
                '}';
    }
}
