package bank;

import java.util.List;

public class Customer {

    private String name;
    private String surname;
    private String pesel;
    private int idPerson;
    private List<Account> accountList;

    public Customer(String name, String surname, String pesel, int idPerson, List<Account> accountList) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.idPerson = idPerson;
        this.accountList = accountList;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPesel() {
        return pesel;
    }

    public int getIdPerson() {
        return idPerson;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

//    public void addAccount(Customer customer, TypeOfAccount typeOfAccount) {
//        Account account = new Account(customer, typeOfAccount );
//        accountList.add(account);
//    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", pesel='" + pesel + '\'' +
                ", idPerson=" + idPerson + '\'' +
                '}';
    }
}
