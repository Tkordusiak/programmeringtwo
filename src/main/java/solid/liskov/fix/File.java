package solid.liskov.fix;

public class File implements FileReadable, FileWrite {
    @Override
    public byte[] read() {
        //reads date
        return new byte[0];
    }

    @Override
    public void write(byte[] date) {
// writes dat
    }
}
