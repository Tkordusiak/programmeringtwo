package pattern.proxy;

public interface CompanyInternetNetwork {
    void getAccess(String username);
}
