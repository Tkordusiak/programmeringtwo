package task1;

import java.util.List;

public class SearchEngine <E extends Creation> {
    private List<E> creations;

    public SearchEngine(List<E> creations) {
        this.creations = creations;
    }

    private E searchCreation(String title) {
        return creations.stream()
                .filter(c -> c.getTitle().equals(title))
                .findAny()
                .orElse((E) Creation.DEFAULT_CREATION);
    }

    public String printCreationInfo(String title) {
        E creation = searchCreation(title);
        //if("".equals(creation.getTitle())) {
        if(Creation.DEFAULT_CREATION == creation) {
            return "Creation not found";
        }
        return creation.getTitle() + " " + creation.getCreator() + " " + creation.getPremiereDate();
    }
}

