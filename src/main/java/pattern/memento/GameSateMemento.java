package pattern.memento;

public class GameSateMemento {

    private int id;
    private String gameState;

    public GameSateMemento(int id, String gameState) {
        this.id = id;
        this.gameState = gameState;
    }

    public int getId() {
        return id;
    }

    public String getGameState() {
        return gameState;
    }
}
