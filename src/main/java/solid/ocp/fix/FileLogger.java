package solid.ocp.fix;

public class FileLogger implements MessageLogger {
    @Override
    public void log(String message) throws Exception {
        // write message to file logger
        System.out.println(message);
    }
}
