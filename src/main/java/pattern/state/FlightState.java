package pattern.state;

public interface FlightState {
    public void updateState(Flight flight);
}
