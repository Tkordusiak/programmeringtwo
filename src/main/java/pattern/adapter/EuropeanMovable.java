package pattern.adapter;

public interface EuropeanMovable {
    double getSpeed();

}
