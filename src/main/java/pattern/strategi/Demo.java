package pattern.strategi;

public class Demo {
    public static void main(String[] args) {
        Travel travel = new Travel("Lańcut", "Wroclaw");
        travel.setTravelPlan(new WalkStrategy(false));
        travel.setTravelPlan(new Carstrategy(true, false));
        travel.setTravelPlan(new Carstrategy(false,true));
    }
}
