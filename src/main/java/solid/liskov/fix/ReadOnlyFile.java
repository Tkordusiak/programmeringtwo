package solid.liskov.fix;

public class ReadOnlyFile implements FileReadable {
    @Override
    public byte[] read() {
        // writes date
        return new byte[0];
    }
}
