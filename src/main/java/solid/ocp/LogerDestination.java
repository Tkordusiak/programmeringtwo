package solid.ocp;

public enum LogerDestination {
    CONSOLE,
    DB,
}
