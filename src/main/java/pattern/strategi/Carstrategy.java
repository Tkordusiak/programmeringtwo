package pattern.strategi;

public class Carstrategy implements TravelStrategy {
    private boolean includePaidMotorways;
    private boolean includeFiledRoads;

    public Carstrategy(boolean includePaidMotorways, boolean includeFiledRoads) {
        this.includePaidMotorways = includePaidMotorways;
        this.includeFiledRoads = includeFiledRoads;
    }

    @Override
    public void setTravelPlan(String from, String to) {
        StringBuilder result = new StringBuilder("Travel by car ")
                .append(includePaidMotorways ? "include paid motorways " : "")
                .append(includeFiledRoads? " imclude filed roads " : "");
        System.out.println(result);

    }
}
