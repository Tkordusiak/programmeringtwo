package solid.liskov.fix;

public interface FileReadable {
    byte[] read();

}
