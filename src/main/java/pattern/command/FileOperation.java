package pattern.command;

@FunctionalInterface
public interface FileOperation {
    String performOperation(String content);
}
