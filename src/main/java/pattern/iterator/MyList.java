package pattern.iterator;

public interface MyList<E> {
    MyIterator<E> iterator();
}
