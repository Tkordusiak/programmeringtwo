package pattern.fasade;

import java.sql.Connection;

public class MySQLReportGenerator {
    public static Connection getMySQLDBConnection(){
        // get MySQL DB connection using connection parameters
        return null;
    }
    public void generateMySQLPDFReport(String tableName, Connection con){
        // get data from table and generate pdf report

    }

    public void generateMySQLHTMLReport(String tableName, Connection con){
        // get data from table and generate html report
    }
}
