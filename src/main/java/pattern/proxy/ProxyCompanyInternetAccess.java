package pattern.proxy;

public class ProxyCompanyInternetAccess implements CompanyInternetNetwork {

    CompanyInternetNetwork internetNetwork;

    @Override
    public void getAccess(String username) {
        if (isEmployees(username)){
            internetNetwork = new  PrivateCompanyInternetNetwork(username);
        }else {
            internetNetwork = new PublicCompanyInternetNetwork(username);
        }
        internetNetwork.getAccess(username);
    }

    private boolean isEmployees(String username){
        return CompanyEmployees.isActiveEmployees(username);
    }
}
