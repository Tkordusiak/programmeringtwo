package bank;

import java.util.ArrayList;
import java.util.List;


public class Bank {


    private List<Customer> clients;
    private List<Account> accountList;

    public Bank(List<Customer> clients, List<Account> accountList) {
        this.clients = clients;
        this.accountList = accountList;
    }

    public void addCustomer(Customer newCustomer) {
        clients.add(newCustomer);

    }
//
//    public void addAccount(Customer customer, TypeOfAccount typeOfAccount) {
//        Account account = new Account(customer, typeOfAccount,);
//        accountList.add(account);
//    }

    public static void deleteAccount(Account account) {
        List<Account> accounts = new ArrayList<>();
        accounts.stream()
                .filter(x -> x.getType().equals(TypeOfAccount.OPEN))
                .findFirst();
        if (account.getBalance() == 0.0) {
            accounts.remove(account);
        }
    }

    public void deleteCustomer(Customer customer, List<Customer> clients, String pesel) {
        List<Customer> customers = new ArrayList<>();
        clients.stream()
                .filter(x-> x.getName().equals("name"))
                .filter(x->x.getSurname().equals("surname"))
                .filter(x -> x.getPesel().equals("pesel"))
                .findFirst();
        if (customer.getPesel() != pesel) {
            customers.remove(customer);
        }
    }

    public void showCustomer(List<Customer> clients) {
        clients.stream()
                .forEach(System.out::println);
    }

}
