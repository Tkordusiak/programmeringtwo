package pattern.adapter;

public abstract class AmericanCar  implements AmericanMovable{

    double speed;

    public double getSpeedMPH(){
        return speed * SpeedConverter.MILES_TO_KILOMETERS.getConverter();
    }
}
