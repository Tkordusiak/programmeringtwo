package solid.ocp.fix;

public class DbLogger implements MessageLogger {

    @Override
    public void log(String message) throws Exception {
       // write message to db
    }
}
