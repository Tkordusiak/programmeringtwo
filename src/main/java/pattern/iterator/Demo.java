package pattern.iterator;

public class Demo {
    public static void main(String[] args) {
        Payment[] payments = {new Payment("woda"), new Payment("Gaz"), new Payment("prad")};
        PaymentList paymentList = new PaymentList(payments);
        MyIterator<Payment> paymentIterator = paymentList.iterator();
        System.out.println("Iterator demo ");
        while (paymentIterator.hasNext()){
            Payment currentPayment = paymentIterator.next();
            System.out.println(currentPayment.getName());
        }
    }
}
