package solid.dip.fix;

public class Test {
    public static void main(String[] args) {

        Repository repository = new FileRepository();
        TaskService taskService = new TaskService(repository);

        taskService.addTask("Task1");
    }
}
