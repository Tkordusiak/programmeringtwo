package pattern.templateMethod;

public class DemoTemplate {
    public static void main(String[] args) {
        NewsProvider provider = new EmailNewsProvider();
        provider.setMessage("New Message");
        provider.provideNews();
    }
}
