package solid.ocp;

public class Logger {
    private LogerDestination destination;

    public Logger(LogerDestination destination) {
        this.destination = destination;
    }

    public void log(String message) throws Exception{
        switch (destination){
            case CONSOLE:
                System.out.println("Print to console: " + message);
                break;
            case DB:
                System.out.println("Save to DB: " + message);
                break;
            default:
                throw new IllegalArgumentException();
        }
    }
}
