package pattern.iterator;

public interface MyIterator<E> {
    E next();

    E current();

    boolean hasNext();
}
