package pattern.memento;

public class Demo {
    public static void main(String[] args) {
        System.out.println("=========");
        GameState gameState = new GameState(1, "Etap 1");

        System.out.println(gameState);
        GameSateMemento memento1 = gameState.saveGame();

        System.out.println(gameState);
        gameState.changeGameState(2, "Etap2");

        System.out.println(gameState);
        gameState.restore(memento1);

        System.out.println(gameState);
    }
}
