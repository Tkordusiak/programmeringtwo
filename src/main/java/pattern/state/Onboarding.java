package pattern.state;

public class Onboarding implements FlightState {
    private static Onboarding instance = new Onboarding();

    public Onboarding() {
    }

    public static Onboarding getInstance() {
        return instance;
    }

    @Override
    public void updateState(Flight flight) {
        System.out.println("Onboarding. Preparing to takeawey!");
        flight.setStatus(Takeaway.getInstance());
    }
}
