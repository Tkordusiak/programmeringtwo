package pattern.chain;

public class Demo {
    public static void main(String[] args) {
        Employees employees = new Employees();
        employees.addEmployees("Jsn Kowal", 10);
        employees.addEmployees("yom kot", 5);
        employees.addEmployees("por kor", 12);


        AccessCheck accessCheck = new CompanyInsiderCheck();
        accessCheck.addChainElement(new EmployeeSeniorityCheck());

        XCompany company = new XCompany();
        company.setCheck(accessCheck);

        System.out.println("Not an employee");
        company.enterRoom("Jan Okoń");
        System.out.println("Employee 1");
        company.enterRoom("por kor");
        System.out.println("Employee 2");
        company.enterRoom("yom kot");
    }

}
