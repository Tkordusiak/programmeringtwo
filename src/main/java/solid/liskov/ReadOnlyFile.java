package solid.liskov;

public class ReadOnlyFile implements FileOperation {
    @Override
    public byte[] read() {
        // writes date
        return new byte[0];
    }

    @Override
    public void write(byte[] date) {
        throw new UnsupportedOperationException();
    }
}
