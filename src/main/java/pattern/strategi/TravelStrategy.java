package pattern.strategi;
@FunctionalInterface

public interface TravelStrategy {

    public void setTravelPlan(String from, String to);
}
