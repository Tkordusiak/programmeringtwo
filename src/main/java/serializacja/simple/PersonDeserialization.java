package serializacja.simple;

import serializacja.Person;
import solid.liskov.File;

import java.io.*;

public class PersonDeserialization {

    public static void main(String[] args) {
        Person person = null;

        try {
            FileInputStream fileInputStream = new FileInputStream("PersonSerialized.data");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            person = (Person) objectInputStream.readObject();

        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(person);

    }
}
