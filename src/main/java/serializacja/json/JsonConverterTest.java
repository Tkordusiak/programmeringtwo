package serializacja.json;

import serializacja.Person;
import serializacja.Persons;

import java.util.ArrayList;
import java.util.List;

public class JsonConverterTest {
    public static void main(String[] args) {

        Person person = new Person("Anna", "Kowalska", 22,"K", 1,"PL");
        Person person2 = new Person("Tom", "Kowal", 34,"K", 1,"PL");
        Person person3 = new Person("Bad", "Ski", 45,"K", 1,"PL");

        List<Person> personsList = new ArrayList<>();
        personsList.add(person);
        personsList.add(person2);
        personsList.add(person3);
        Persons people = new Persons();
        people.setPeople(personsList);

        PeopleJsonUtil.mapPeopleJson(people);
        PeopleJsonUtil.savePeopleToJsonFile(people);
        Persons readPersons = PeopleJsonUtil.readPeopleFromJsonFile("peopleJson.json");
        System.out.println(readPersons);
        readPersons.getPeople().forEach(System.out::println);




//        Person person = new Person("Jan", "Kowalski", 35, "M", 1, "PL");
//        PersonJsonUtil.mapPersonJson(person);
//
//        PersonJsonUtil.savePersonToJsonFile(person);
//
//        Person personRead = PersonJsonUtil.readPersonFromJsonFile("personJson.json");
//        System.out.println("=============");
//        System.out.println(personRead);
    }
}
