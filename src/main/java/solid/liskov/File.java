package solid.liskov;

public class File implements FileOperation {
    @Override
    public byte[] read() {
        //reads date
        return new byte[0];
    }

    @Override
    public void write(byte[] date) {
// writes dat
    }
}
