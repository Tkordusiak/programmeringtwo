package pattern.singleton;

public class LazySingleton {
    private static  LazySingleton instatnce;

    private LazySingleton(){
        System.out.println("Lazy singleton initialization");
    }
    public  static LazySingleton getInstance(){
        if (instatnce == null){
            instatnce = new LazySingleton();
        }
        return instatnce;
    }
}
