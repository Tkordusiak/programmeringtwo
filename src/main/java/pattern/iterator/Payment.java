package pattern.iterator;

public class Payment {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Payment(String name) {
        this.name = name;
    }
}
