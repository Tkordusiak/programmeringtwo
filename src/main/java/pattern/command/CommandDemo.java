package pattern.command;

public class CommandDemo {
    public static void main(String[] args) {
        MyFile file =  new MyFile("sample,txt");

        // stara wersja
        file.createFile("zawartość pliki");

        // wzorzec command
        FileOperationPerformer performer  = new FileOperationPerformer();
        performer.executeOperation(new CreateFileOperation(file), "zawartość pliku");

        performer.executeOperation(new UpdateFileOperation(file), "dopisanie ");
    }
}
