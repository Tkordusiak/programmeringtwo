package solid.liskov;

public interface FileOperation {
    byte[] read();

    void write(byte[] date);
}
