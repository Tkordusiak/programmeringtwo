package pattern.chain;

public class EmployeeSeniorityCheck extends AccessCheck {
    @Override
    public boolean doCheck(String username) {
        System.out.println("Employee seniority check stat for " + username);
        if (Employees.checkEmployeeLevel(username)<10){
            System.out.println(getClass().getCanonicalName() + " do check FAILED");
        }
        System.out.println(getClass().getCanonicalName() + " do check PASSED");
        return checkNextElement(username);
    }
}
