package pattern.state;

public class Landed implements FlightState {

    private static Landed instance = new Landed();

    public Landed() {
    }

    public static Landed getInstance() {
        return instance;
    }

    @Override
    public void updateState(Flight flight) {
        System.out.println("Landed. Enjoy your stay ");
    }
}
