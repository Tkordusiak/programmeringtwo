package pattern.chain;

public class CompanyInsiderCheck extends AccessCheck {
    @Override
    public boolean doCheck(String username) {
        System.out.println("Company insider check start for " + username);
        if (!Employees.checkEmployee(username)){
            System.out.println(getClass().getCanonicalName() + " do chceck FAILED");
            return false;
        }
        System.out.println(getClass().getCanonicalName() + " do check PASSED");
        return checkNextElement(username);
    }
}
