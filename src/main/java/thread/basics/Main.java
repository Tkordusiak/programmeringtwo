package thread.basics;

import static thread.ThreadColor.*;

public class Main {
    public static void main(String[] args) {
        System.out.println(ANSI_PURPLE + " Main thread in action");

        Thread otherThread = new OtherThread();
        otherThread.setName("====OTHER THREAD======");
        otherThread.start();  // startuje nowy wątek i wykonuje to co jest w run w nowym watku
        //otherThread.run();  // wykonuje to co jest w OtherThread ale nie generuje nowego wątku

        Thread runnableSample = new Thread(new RunnableSampleThread());
        runnableSample.start();


        System.out.println(ANSI_PURPLE + " End of main");

        //otherThread.interrupt(); // wywoła ptzerwanie sleepa na wątku

        // w przypadku gdy mamy niewielki task i nie ma sensu pisać do niego
        // osobnej dedykowanej klase ale chcemu
        // go zrobić w osobnym wątku robimy klasę anonimową

        new Thread() {
            @Override
            public void run() {
                System.out.println(ANSI_CYAN + " Anonymous class thread!");
            }
        }.start();

        // nadpisanie klasy na potrzebe pojedynczej akcji
        runnableSample = new Thread(new RunnableSampleThread() {
            @Override
            public void run() {
                System.out.println(ANSI_RED + " overridden RunnableSampleThread");
                //przykład na łączenie dwóch wątków,gdzie jeden czeka na zakończenie działania drugiego
           try {
                otherThread.join();
               System.out.println(ANSI_RED + " Other thread finished so I continue");
           }catch (InterruptedException e){
               System.out.println(ANSI_RED + " overridden Runnable  terminated");
           }
            }
        });
        runnableSample.start();
    }
}
