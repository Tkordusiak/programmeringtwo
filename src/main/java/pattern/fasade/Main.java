package pattern.fasade;

import java.sql.Connection;

public class Main {
    public static void main(String[] args) {

        String tableName = "Employee";

        // generating MySQL HTML report and Oracle PDF report without using Facade
        Connection conMySQL = MySQLReportGenerator.getMySQLDBConnection();
        MySQLReportGenerator mySQLReportGenerator = new MySQLReportGenerator();
        mySQLReportGenerator.generateMySQLHTMLReport(tableName, conMySQL);

        Connection conOracle = OracleReportGenerator.geOracleDBConnection();
        OracleReportGenerator oracleReportGenerator = new OracleReportGenerator();
        oracleReportGenerator.generateOracleHTMLReport(tableName, conOracle);

        System.out.println("=================");

        // generating MySQL HTML and Orcle PDF report using Facade
        Facade.generateReport(Facade.DBType.MYSQL, Facade.ReportType.HTML, tableName);
        Facade.generateReport(Facade.DBType.ORACLE, Facade.ReportType.PDF, tableName);

    }
}
