package serializacja.xmlToCar;

import serializacja.Car;
import serializacja.Person;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.StringWriter;
import java.util.Optional;

public class CarToXmlConvert {

    public static void CarToXml(Car car){
      try {
          JAXBContext context = JAXBContext.newInstance(Car.class);
          Marshaller marshaller = context.createMarshaller();
          marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
          StringWriter stringWriter = new StringWriter();
      }catch (JAXBException e){
          e.printStackTrace();
      }

    }

    public static void CarToXmlFile(Car car){
        try {
            JAXBContext context = JAXBContext.newInstance(Car.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            File file = new File("carXML.xml");
            marshaller.marshal(car, file);
        }catch (JAXBException e){
            e.printStackTrace();
        }
    }
    public static Optional<Car> xmlFileToCar(String fileName){
        File xmlFile  = new File(fileName);
        JAXBContext jaxbContext;
        Car car = null;
        try {
            jaxbContext = JAXBContext.newInstance(Car.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            car = (Car) unmarshaller.unmarshal(xmlFile);
        }catch (JAXBException e){
            e.printStackTrace();
        }
        return Optional.ofNullable(car);
    }
}
