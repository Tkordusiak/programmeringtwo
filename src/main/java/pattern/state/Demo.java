package pattern.state;

public class Demo {
    public static void main(String[] args) {
        Flight flight = new Flight();
        flight.update();
        flight.update();
        flight.update();
        flight.update();
        flight.update();
        flight.update();
        flight.update();

    }
}
