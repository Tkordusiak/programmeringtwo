package pattern.fasade;

import java.sql.Connection;

public class Facade {

    public static void generateReport(DBType dbType, ReportType reportType, String tableName) {
        Connection con = null;
        switch (dbType) {
            case MYSQL:
                con = MySQLReportGenerator.getMySQLDBConnection();
                MySQLReportGenerator mySQLReportGenerator = new MySQLReportGenerator();
                switch (reportType) {
                    case HTML:
                        mySQLReportGenerator.generateMySQLHTMLReport(tableName, con);
                        break;
                    case PDF:
                        mySQLReportGenerator.generateMySQLPDFReport(tableName, con);
                        break;
                }
                break;
            case ORACLE:
                con = OracleReportGenerator.geOracleDBConnection();
                OracleReportGenerator oracleReportGenerator = new OracleReportGenerator();
                switch (reportType) {
                    case HTML:
                        oracleReportGenerator.generateOracleHTMLReport(tableName, con);
                        break;
                    case PDF:
                        oracleReportGenerator.generateOraclePDFReport(tableName, con);
                        break;
                }
                break;
        }
    }

    public enum DBType {
        MYSQL, ORACLE;
    }

    public enum ReportType {
        HTML, PDF;
    }
}
